using Cake.Core.IO;
using Cake.Core.Tooling;

namespace Ignis.IisExpress.Cake
{
    public sealed class IisExpressSettings : ToolSettings
    {
        public bool? SystemTray { get; set; }
        public FilePath ConfigurationFile { get; set; }
        public string Site { get; set; }
        public int? Port { get; set; }
        public string Path { get; set; }
        public FilePath LogFile { get; set; }

        internal void ImportIisExpressArgumentsTo(ProcessArgumentBuilder args)
        {
            new ArgumentBuilder(args)
                .Append(SystemTray, value => $"/systray:{value.ToString().ToLowerInvariant()}")
                .Append(ConfigurationFile, value => $"/config:{value}")
                .Append(Site, value => $"/site:{value}")
                .Append(Port, value => $"/port:{value}")
                .Append(Path, value => $"/path:{value}");
        }

        public void ImportCliArgumentsTo(ProcessArgumentBuilder args)
        {
            new ArgumentBuilder(args)
                .Append(LogFile, value => $"--log-file={value}");
        }
    }
}