using Cake.Core.IO;

namespace Ignis.IisExpress.Cake
{
    internal sealed class ArgumentsBuilder
    {
        private readonly CliCommand _command;
        private readonly IisExpressSettings _settings;

        public ArgumentsBuilder(CliCommand command, IisExpressSettings settings)
        {
            _command = command;
            _settings = settings;
        }

        public ProcessArgumentBuilder Build()
        {
            var args = new ProcessArgumentBuilder();

            _settings.ImportCliArgumentsTo(args);
            _command.ImportTo(args);
            _settings.ImportIisExpressArgumentsTo(args);

            return args;
        }
    }
}