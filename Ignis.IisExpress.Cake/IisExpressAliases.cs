﻿using Cake.Core;
using Cake.Core.Annotations;

namespace Ignis.IisExpress.Cake
{
    public static class IisExpressAliases
    {
        #region IisExpressStart

        [CakeMethodAlias]
        public static void IisExpressStart(this ICakeContext context)
        {
            IisExpressStart(context, new IisExpressSettings());
        }

        [CakeMethodAlias]
        public static void IisExpressStart(this ICakeContext context, IisExpressSettings settings)
        {
            new IisExpressRunner(context).Run(CliCommand.Start, settings);
        }

        #endregion

        #region IisExpressStop

        [CakeMethodAlias]
        public static void IisExpressStop(this ICakeContext context)
        {
            IisExpressStop(context, new IisExpressSettings());
        }

        [CakeMethodAlias]
        public static void IisExpressStop(this ICakeContext context, IisExpressSettings settings)
        {
            new IisExpressRunner(context).Run(CliCommand.Stop, settings);
        }

        #endregion
    }
}