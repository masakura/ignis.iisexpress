﻿#addin nuget:?package=Cake.Http&version=0.7.0
#addin nuget:?package=Cake.FileHelpers&version=3.2.1
#load ../cake/versions.cake

var target = Argument("target", (string) null);

var outputPack = new DirectoryPath(Argument("output-pack", "dist")).MakeAbsolute(Context.Environment);

Task("Publish")
    .Does(() => {
        DotNetCorePublish(".", new DotNetCorePublishSettings
            {
                OutputDirectory = "publish", 
            });
    });

Task("Pack")
    .Does(() => {
        var settings = new DotNetCorePackSettings
        {
            Configuration = "Release",
            OutputDirectory = outputPack,
        };
        versions.ImportTo(settings);
       
        DotNetCorePack(".", settings);
    });

Task("Pack:Cli")
    .Does(() => {
        CakeExecuteScript("../Ignis.IisExpress.Cli/build.cake", new CakeSettings
            {
                ArgumentCustomization = args => args.Append("-target=Pack"),
            });
    });

void IisExpress(string target)
{
    var script = new StringBuilder();
    var nuget = new DirectoryPath("../Ignis.IisExpress.Cli/dist").MakeAbsolute(Context.Environment);
    var addin = new FilePath("./publish/Ignis.IisExpress.Cake.dll").MakeAbsolute(Context.Environment);
    script.AppendLine($"#tool nuget:{nuget}?package=Ignis.IisExpress.Cli&prerelease&version={versions.PackageVersion()}");
    script.AppendLine($"#reference {addin}");
    Information(script.ToString());
    script.Append(FileReadText("./cake-script-test/template.cake"));

    var wwwroot = new DirectoryPath("./cake-script-test").MakeAbsolute(Context.Environment).ToString()
        .Replace("/", @"\");
    var settings = new CakeSettings
    {
        Arguments = new Dictionary<string, string>
        {
            { "target", target },
            { "wwwroot", wwwroot },
        },
    };

    CakeExecuteExpression(script.ToString(), settings);
}

Task("IIS Express:Start")
    .Does(() => IisExpress("start"));

Task("IIS Express:Stop")
    .Does(() => IisExpress("stop"));
    
Task("Test")
    .IsDependentOn("Pack:Cli")
    .IsDependentOn("Publish")
    .IsDependentOn("IIS Express:Start")
    .Does(() => {
        var responseBody = HttpGet("http://localhost:8000/");
        if (!responseBody.Contains("Cake IIS Express addin test page"))
            throw new CakeException("IIS Express が起動しませんでした");
    })
    .Finally(() => RunTarget("IIS Express:Stop"));

RunTarget(target);