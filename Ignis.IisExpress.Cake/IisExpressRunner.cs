using System;
using System.Collections.Generic;
using Cake.Core;
using Cake.Core.Tooling;

namespace Ignis.IisExpress.Cake
{
    internal sealed class IisExpressRunner : Tool<IisExpressSettings>
    {
        public IisExpressRunner(ICakeContext context) :
            base(context.FileSystem, context.Environment, context.ProcessRunner, context.Tools)
        {
        }

        public void Run(CliCommand command, IisExpressSettings settings)
        {
            var args = new ArgumentsBuilder(command, settings)
                .Build();

            Console.WriteLine(args.Render());

            Run(settings, args);
        }

        protected override string GetToolName()
        {
            return "Ignis.IisExpress.Cli";
        }

        protected override IEnumerable<string> GetToolExecutableNames()
        {
            return new[] {"Ignis.IisExpress.Cli.exe"};
        }
    }
}