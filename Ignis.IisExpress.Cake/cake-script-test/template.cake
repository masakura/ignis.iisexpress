﻿var target = Argument("target", (string) null);
var wwwroot = Argument("wwwroot", (string) null);

IisExpressSettings CreateDefault()
{
    return new IisExpressSettings();
}

Task("Start")
    .Does(() => {
        var settings = CreateDefault();
        settings.Path = wwwroot;
        settings.SystemTray = false;
        settings.Port = 8000;
        settings.LogFile = "logfile.log";
        IisExpressStart(settings);
    });

Task("Stop")
    .Does(() => {
        var settings = CreateDefault();
        IisExpressStop(settings);
    });

RunTarget(target);