using System;
using Cake.Core;
using Cake.Core.IO;

namespace Ignis.IisExpress.Cake
{
    internal sealed class ArgumentBuilder
    {
        private readonly ProcessArgumentBuilder _arguments;

        public ArgumentBuilder(ProcessArgumentBuilder arguments)
        {
            _arguments = arguments;
        }

        public ArgumentBuilder Append<T>(T value, Func<T, string> argumentBuilder) where T : class
        {
            if (value != null) _arguments.Append(argumentBuilder(value));

            return this;
        }

        public ArgumentBuilder Append<T>(T? value, Func<T, string> argumentBuilder)
            where T : struct
        {
            if (value != null) _arguments.Append(argumentBuilder(value.Value));

            return this;
        }
    }
}