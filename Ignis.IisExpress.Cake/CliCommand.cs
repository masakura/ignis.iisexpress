using Cake.Core;
using Cake.Core.IO;

namespace Ignis.IisExpress.Cake
{
    internal sealed class CliCommand
    {
        private readonly string _command;

        private CliCommand(string command)
        {
            _command = command;
        }

        public static CliCommand Start { get; } = new CliCommand("start");
        public static CliCommand Stop { get; } = new CliCommand("stop");

        public override string ToString()
        {
            return _command;
        }

        public void ImportTo(ProcessArgumentBuilder args)
        {
            args.Append(_command);
        }
    }
}