﻿class Versions
{
    private readonly string _version;
    private readonly string _suffix;
    private readonly string _build;
    private readonly bool _release;
    
    public Versions(string version, string suffix, string build, bool release)
    {
        _version = version;
        _suffix = suffix;
        _build = build;
        _release = release;
    }
    
    // TODO NormalizedVersion かな?
    public string Normalized()
    {
        return _version.Replace("v", "");
    }
    
    public string Suffix()
    {
        if (_release) return "";
        if (string.IsNullOrEmpty(_suffix)) return "";

        var s = _suffix;
        if (!string.IsNullOrEmpty(_build)) s += $".{_build}";

        return s;
    }
    
    public string FileVersion()
    {
        var s = Normalized();
        if (!string.IsNullOrEmpty(_build)) s += $".{_build}";
        
        return s;
    }
    
    public string PackageVersion()
    {
        var s = Normalized();
        var suffix = Suffix();
        if (!string.IsNullOrEmpty(suffix)) s += $"-{suffix}";
        
        return s;
    }
    
    public ProcessArgumentBuilder Embed(ProcessArgumentBuilder args)
    {
        return args
            .Append($"--product-version={_version}")
            .Append($"--product-version-suffix={_suffix}")
            .Append($"--product-build-version={_build}")
            .Append($"--release-version={_release}");
    }
    
    public void ImportTo(DotNetCorePackSettings settings)
    {
        var previous = settings.ArgumentCustomization;
        if (previous == null) previous = (ProcessArgumentBuilder args) => args;
         
        settings.ArgumentCustomization = args => previous(args)
            .Append($"-p:VersionPrefix={Normalized()}")
            .Append($"-p:VersionSuffix={Suffix()}");

        settings.VersionSuffix = Suffix();
    }
}

var versions = new Versions(Argument("product-version", "0.1.0"),
    Argument("product-version-suffix", "nora"),
    Argument("product-build-version", "0"),
    Argument("release-version", false));
