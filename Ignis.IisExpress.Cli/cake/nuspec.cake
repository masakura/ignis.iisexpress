﻿public sealed class Nuspec
{
    private readonly string _xml;

    public Nuspec(string xml)
    {
        _xml = xml;
    }
    
    public static Nuspec Load(string path)
    {
        return new Nuspec(System.IO.File.ReadAllText(path));
    }
    
    public void Save(string path)
    {
        System.IO.File.WriteAllText(path, _xml);
    }
    
    public Nuspec Version(string version)
    {
        var newXml = System.Text.RegularExpressions
            .Regex.Replace(_xml, "<version>.*", $"<version>{version}</version>");
        
        return new Nuspec(newXml);
    }
}