using System.Threading;
using System.Threading.Tasks;
using Ignis.IisExpress.Wrapper.Streams;

namespace Ignis.IisExpress.Cli
{
    /// <summary>
    ///     IIS Express の標準出力を読み込んで、起動されるまで待機します。
    /// </summary>
    internal class WaitIisExpressStarted : IOutput
    {
        private readonly ManualResetEvent _event = new ManualResetEvent(false);

        public Task WriteLineAsync(string value)
        {
            if (value.StartsWith("IIS Express is running."))
                _event.Set();

            return Task.CompletedTask;
        }

        public async Task WaitStartedAsync()
        {
            await Task.Yield();

            _event.WaitOne();
        }
    }
}