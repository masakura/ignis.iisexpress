using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using Ignis.IisExpress.Wrapper.Streams;

namespace Ignis.IisExpress.Cli
{
    internal static class Program
    {
        private static async Task Main(string[] args)
        {
            var wrapper = KickIisExpressWrapper(args);

            var waiter = new WaitIisExpressStarted();

            var task = new ProcessStreamPipe(wrapper)
                .AppendStandardOutput(waiter)
                .RunAsync();

            await Task.WhenAny(task, waiter.WaitStartedAsync());
        }

        private static Process KickIisExpressWrapper(string[] args)
        {
            var path = Path.GetDirectoryName(typeof(Program).Assembly.Location);
            Debug.Assert(path != null, nameof(path) + " != null");
            var wrapper = Path.Combine(path, "Ignis.IisExpress.Wrapper.exe");

            var info = new ProcessStartInfo(wrapper)
            {
                CreateNoWindow = true,
                UseShellExecute = false,
                RedirectStandardOutput = true,
                RedirectStandardError = true
            };
            foreach (var arg in args) info.ArgumentList.Add(arg);

            var process = Process.Start(info);
            return process;
        }
    }
}