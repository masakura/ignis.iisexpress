﻿#load ../cake/versions.cake
#load ./cake/nuspec.cake

﻿var target = Argument("target", (string) null);

var outputPack = new DirectoryPath(Argument("output-pack", "dist")).MakeAbsolute(Context.Environment);

Task("Publish")
    .Does(() => DotNetCorePublish(".", new DotNetCorePublishSettings
        {
            ArgumentCustomization = args => args
                .Append($"-p:FileVersion={versions.FileVersion()}")
                .Append($"-p:Version={versions.PackageVersion()}"),
            Configuration = "Release",
            OutputDirectory = "nupkg/tool",
        }));

Task("Pack")
    .IsDependentOn("Publish")
    .Does(() => {
        Nuspec.Load("Ignis.IisExpress.Cli.nuspec")
            .Version(versions.PackageVersion())
            .Save("./nupkg/temp.nuspec");

        DotNetCorePack(".", new DotNetCorePackSettings
        {
            ArgumentCustomization = args => args
                .Append($"-p:NoBuild=true")
                .Append(@"-p:NuspecFile=.\nupkg\temp.nuspec"),
            OutputDirectory = outputPack,
        });
    });

RunTarget(target);