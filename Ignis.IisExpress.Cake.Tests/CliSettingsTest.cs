using Cake.Core.IO;
using Xunit;

namespace Ignis.IisExpress.Cake
{
    public sealed class CliSettingsTest
    {
        public CliSettingsTest()
        {
            _args = new ProcessArgumentBuilder();
            _target = new IisExpressSettings();
        }

        private readonly ProcessArgumentBuilder _args;
        private readonly IisExpressSettings _target;

        [Fact]
        public void TestLogFile()
        {
            _target.LogFile = "reports/logfile.log";
            _target.ImportCliArgumentsTo(_args);

            Assert.Equal("--log-file=reports/logfile.log", _args.Render());
        }
    }
}