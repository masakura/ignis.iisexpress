using Cake.Core.IO;
using Xunit;

namespace Ignis.IisExpress.Cake
{
    public sealed class IisExpressSettingsTest
    {
        public IisExpressSettingsTest()
        {
            _args = new ProcessArgumentBuilder();
            _target = new IisExpressSettings();
        }

        private readonly ProcessArgumentBuilder _args;
        private readonly IisExpressSettings _target;

        [Fact]
        public void TestConfigurationFile()
        {
            _target.ConfigurationFile = @"c:\path\to\applicationhost.config";

            _target.ImportIisExpressArgumentsTo(_args);

            Assert.Equal("/config:c:/path/to/applicationhost.config", _args.Render());
        }

        [Fact]
        public void TestNoArguments()
        {
            _target.ImportIisExpressArgumentsTo(_args);

            Assert.Equal(string.Empty, _args.Render());
        }

        [Fact]
        public void TestPath()
        {
            _target.Path = @"C:\path\to\wwwroot";

            _target.ImportIisExpressArgumentsTo(_args);

            Assert.Equal(@"/path:C:\path\to\wwwroot", _args.Render());
        }

        [Fact]
        public void TestPort()
        {
            _target.Port = 8080;

            _target.ImportIisExpressArgumentsTo(_args);

            Assert.Equal("/port:8080", _args.Render());
        }

        [Fact]
        public void TestSite()
        {
            _target.Site = "webapp";

            _target.ImportIisExpressArgumentsTo(_args);

            Assert.Equal("/site:webapp", _args.Render());
        }

        [Fact]
        public void TestSystemTray()
        {
            _target.SystemTray = true;

            _target.ImportIisExpressArgumentsTo(_args);

            Assert.Equal("/systray:true", _args.Render());
        }
    }
}