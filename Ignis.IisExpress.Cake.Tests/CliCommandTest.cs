using Cake.Core.IO;
using Xunit;

namespace Ignis.IisExpress.Cake
{
    public sealed class CliCommandTest
    {
        public CliCommandTest()
        {
            _args = new ProcessArgumentBuilder();
        }

        private readonly ProcessArgumentBuilder _args;

        [Fact]
        public void TestStart()
        {
            CliCommand.Start.ImportTo(_args);

            Assert.Equal("start", _args.Render());
        }

        [Fact]
        public void TestStop()
        {
            CliCommand.Stop.ImportTo(_args);

            Assert.Equal("stop", _args.Render());
        }
    }
}