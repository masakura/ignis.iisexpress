using Xunit;

namespace Ignis.IisExpress.Cake
{
    public sealed class ArgumentsBuilderTest
    {
        [Fact]
        public void TestBuild()
        {
            var actual = new ArgumentsBuilder(CliCommand.Start, new IisExpressSettings
                {
                    LogFile = "logfile.log",
                    Path = "wwwroot"
                })
                .Build();

            Assert.Equal("--log-file=logfile.log start /path:wwwroot", actual.Render());
        }
    }
}