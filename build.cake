#tool JetBrains.ReSharper.CommandLineTools&version=2020.1.3
#tool ReSharperReports&version=0.4.0
#addin Cake.ReSharperReports&version=0.11.1
#load ./cake/versions.cake

var target = Argument("target", (string) null);
var output = new DirectoryPath(Argument("output", "dist")).MakeAbsolute(Context.Environment);

var codeInspections = new FilePath("reports/code-inspections.xml");
var testReports = new FilePath("reports/test-results.xml").MakeAbsolute(Context.Environment);
var solution = new FilePath("Ignis.IisExpress.sln");

Task("Restore")
	.Does(() => DotNetCoreRestore());

Task("Lint")
	.IsDependentOn("Restore")
	.Does(() => {
		var settings = new InspectCodeSettings
		{
			OutputFile = codeInspections,
			ThrowExceptionOnFindingViolations = true,
		};
		InspectCode(solution, settings);
	})
	.Finally(() => ReSharperReports(codeInspections, "reports/code-inspections.html"));

Task("Test")
    .Does(() => {
        var settings = new DotNetCoreTestSettings
        {
            Logger = $"junit;LogFilePath={testReports}",
        };
        
        DotNetCoreTest(solution.ToString(), settings);
    });
    
Task("Clean")
    .Does(() => CleanDirectory(output));

Task("Pack")
    .IsDependentOn("Clean")
    .Does(() => {
        var settings = new CakeSettings
        {
            ArgumentCustomization = args => versions.Embed(args)
                .Append("--target=Pack")
                .Append($"--output-pack={output}")
        };
        
        CakeExecuteScript("Ignis.IisExpress.Cli/build.cake", settings);
        CakeExecuteScript("Ignis.IisExpress.Cake/build.cake", settings);
    });

Task("PackWithTest")
    .IsDependentOn("Pack")
    .Does(() => {
        var script = new StringBuilder();
        script.AppendLine($"#tool nuget:{output}?package=Ignis.IisExpress.Cli&prerelease");
        script.AppendLine($"#addin nuget:{output}?package=Ignis.IisExpress.Cake&prerelease");

        Information(script.ToString());
        
        CakeExecuteExpression(script.ToString(), new CakeSettings
        {
            EnvironmentVariables = new Dictionary<string, string>
            {
                { "CAKE_SETTINGS_SKIPPACKAGEVERSIONCHECK",  "true" }
            },
            Verbosity = Verbosity.Diagnostic,
        });
    });


RunTarget(target);