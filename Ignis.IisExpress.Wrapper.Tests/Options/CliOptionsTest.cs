using Xunit;

namespace Ignis.IisExpress.Wrapper.Options
{
    public sealed class CliOptionsTest
    {
        [Fact]
        public void TestDefaultLogFileOptions()
        {
            var result = CliOptions.Parse(new string[] { });

            Assert.Equal("iisexpress.log", result.LogFile);
        }

        [Fact]
        public void TestDefaultPidFileOptions()
        {
            var result = CliOptions.Parse(new string[] { });

            Assert.Equal("iisexpress.pid", result.PidFile);
        }
    }
}