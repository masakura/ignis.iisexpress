using Xunit;

namespace Ignis.IisExpress.Wrapper.Options
{
    public sealed class IisExpressCliOptionsTest
    {
        private readonly IisExpressCliCommandArgumentsEqualityComparer _comparer =
            new IisExpressCliCommandArgumentsEqualityComparer();

        [Fact]
        public void TestCliOptions()
        {
            var options = IisExpressCliOptions.Parse(new[]
            {
                "--pid-file=sample.pid",
                "--log-file=sample.log",
                "start"
            });

            var actual = new
            {
                options.CliOptions.PidFile, options.CliOptions.LogFile
            };

            Assert.Equal(actual, new
            {
                PidFile = "sample.pid",
                LogFile = "sample.log"
            });
        }

        [Fact]
        public void TestEmptyIsHelpCommand()
        {
            var options = IisExpressCliOptions.Parse(new string[] { });

            Assert.Equal("help", options.CommandName);
        }

        [Fact]
        public void TestStartCommand()
        {
            var options = IisExpressCliOptions.Parse(new[] {"start", @"/path:c:\temp"});

            Assert.Equal(new IisExpressCliOptions(null, "start", new IisExpressOptions(new[] {@"/path:c:\temp"})),
                options,
                _comparer);
        }
    }
}