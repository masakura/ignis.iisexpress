using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;

namespace Ignis.IisExpress.Wrapper.Processes
{
    internal static class ProcessHelper
    {
        public static Process GetRunningProcess()
        {
            var process = StartProcess();
            Debug.Assert(process != null, nameof(process) + " != null");
            return process;
        }

        public static int GetNotRunningProcessId()
        {
            var ids = Enumerable.Range(1, 65536);
            var runningProcessIds = Process.GetProcesses().Select(process => process.Id);

            return ids.Except(runningProcessIds).First();
        }

        private static Process StartProcess()
        {
            return Process.Start(ProcessName());
        }

        private static string ProcessName()
        {
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows)) return "cmd";

            return "sh";
        }
    }
}