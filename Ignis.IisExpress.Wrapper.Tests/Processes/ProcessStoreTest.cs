using System;
using System.Diagnostics;
using Moq;
using Xunit;

namespace Ignis.IisExpress.Wrapper.Processes
{
    public sealed class ProcessStoreTest : IDisposable
    {
        public ProcessStoreTest()
        {
            _mockStore = new Mock<IStore>(MockBehavior.Strict);
            _target = new ProcessStore(_mockStore.Object);
            _process = Process.GetCurrentProcess();
        }

        public void Dispose()
        {
            _mockStore.VerifyAll();
        }

        private readonly Mock<IStore> _mockStore;
        private readonly ProcessStore _target;
        private readonly Process _process;

        [Fact]
        public void TestClear()
        {
            _mockStore.Setup(x => x.Exists()).Returns(true);
            _mockStore.Setup(x => x.Delete());

            _target.Clear();
        }

        [Fact]
        public void TestLoad()
        {
            _mockStore.Setup(x => x.Exists()).Returns(true);
            _mockStore.Setup(x => x.Load()).Returns(_process.Id);

            var result = _target.Load();

            Assert.Equal(_process.Id, result.Id);
        }

        [Fact]
        public void TestLoadIsNotExist()
        {
            _mockStore.Setup(x => x.Exists()).Returns(false);

            var result = _target.Load();

            Assert.False(result.IsRunning);
        }

        [Fact]
        public void TestSave()
        {
            _mockStore.Setup(x => x.Save(_process.Id));

            _target.Save(_process);
        }
    }
}