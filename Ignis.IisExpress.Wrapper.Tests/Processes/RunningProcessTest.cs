using System.Threading;
using Xunit;

namespace Ignis.IisExpress.Wrapper.Processes
{
    public sealed class RunningProcessTest
    {
        [Fact]
        public void TestIsNotRunning()
        {
            var processId = ProcessHelper.GetNotRunningProcessId();
            var target = new RunningProcess(processId);

            Assert.False(target.IsRunning);
        }

        [Fact]
        public void TestIsNotRunningKilled()
        {
            using var process = ProcessHelper.GetRunningProcess();
            var target = new RunningProcess(process.Id);

            process.Kill();
            Thread.Sleep(1000);

            Assert.False(target.IsRunning);
        }

        [Fact]
        public void TestIsRunning()
        {
            using var process = ProcessHelper.GetRunningProcess();
            var target = new RunningProcess(process.Id);

            try
            {
                Assert.True(target.IsRunning);
            }
            finally
            {
                process.Kill();
            }
        }
    }
}