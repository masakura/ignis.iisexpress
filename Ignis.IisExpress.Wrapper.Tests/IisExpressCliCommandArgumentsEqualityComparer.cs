using System;
using System.Collections.Generic;
using System.Linq;
using Ignis.IisExpress.Wrapper.Options;

namespace Ignis.IisExpress.Wrapper
{
    internal class IisExpressCliCommandArgumentsEqualityComparer : IEqualityComparer<IisExpressCliOptions>
    {
        public bool Equals(IisExpressCliOptions x, IisExpressCliOptions y)
        {
            if (x == null && y == null) return true;
            if (x == null || y == null) return false;

            if (x.Equals(y)) return true;

            return x.CommandName.Equals(y.CommandName) &&
                   x.IisExpressOptions.Arguments.SequenceEqual(y.IisExpressOptions.Arguments);
        }

        public int GetHashCode(IisExpressCliOptions obj)
        {
            return HashCode.Combine(obj.CommandName.GetHashCode(), GetArrayHashCode(obj.IisExpressOptions.Arguments));
        }

        private static int GetArrayHashCode<T>(IEnumerable<T> items)
        {
            return items.Aggregate(0, (current, item) => (current * 397) ^ (item?.GetHashCode() ?? 0));
        }
    }
}