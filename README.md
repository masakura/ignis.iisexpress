# IIS Express Tools
## 目的
Cake Script から `iisexpress.exe` の起動や終了を助けます。


## Ignis.IisExpress.Cake
Cake ファイルにディレクティブを追加します。

```csharp
#tool nuget:https://gitlab.com/api/v4/projects/18928089/packages/nuget/index.json?package=Ignis.IisExpress.Cli&version=0.5.0
#addin nuget:https://gitlab.com/api/v4/projects/18928089/packages/nuget/index.json?package=Ignis.IisExpress.Cake
```

`IisExpressStart()` メソッドで IIS Express が起動します。IIS Express の起動が完了したらこの関数から戻ってきます。

```csharp
IisExpressStart(new IisExpressSettings
    {
        Port = 8080,
        Path = @"c:\path\to\wwwroot",
    });
```

`IisExpressStop()` メソッドで IIS Express を終了します。起動した IIS Express のプロセス ID は記録されており、それを利用してプロセスを終了させます。

```csharp
IisExpressStop();
```


## Ignis.IisExpress.Cli
IIS Express の起動や終了をする CLI。

`iisexpress.exe` は起動するとコンソールに戻ってきません。また、終了方法も `Ctrl + Q` を押す必要があり、バッチプログラムから起動や終了を行うことが難しいです。

`start` コマンドで IIS Express を起動します。IIS Express の起動が完了したらコンソールに戻ってきます。

```
> Ignis.IisExpress.Clie start /path:c:\path\to\wwwroot
```

`stop` コマンドで IIS Express を終了します。IIS Express のプロセス ID を記憶しており、それを利用します。

```
> Ignis.IisExpress.Clie stop
```

詳しくはヘルプを見てください。

```
> Ignis.IisExpress.Clie help
```