using System;
using System.Threading.Tasks;
using Ignis.IisExpress.Wrapper.Commands;
using Ignis.IisExpress.Wrapper.Options;

namespace Ignis.IisExpress.Wrapper
{
    internal static class Program
    {
        private static async Task Main(string[] args)
        {
            var options = IisExpressCliOptions.Parse(args);
            var command = new CommandProvider().Get(options);
            await command.RunAsync();

            Environment.Exit(Environment.ExitCode);
        }
    }
}