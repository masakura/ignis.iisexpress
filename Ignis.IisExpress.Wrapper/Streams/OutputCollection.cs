using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;

namespace Ignis.IisExpress.Wrapper.Streams
{
    internal sealed class OutputCollection
    {
        private readonly IEnumerable<IOutput> _items;

        public OutputCollection() : this(Enumerable.Empty<IOutput>())
        {
        }

        private OutputCollection(IEnumerable<IOutput> items)
        {
            _items = items.ToImmutableArray();
        }

        public OutputCollection Append(IOutput output)
        {
            var items = _items.Union(new[] {output});
            return new OutputCollection(items);
        }

        public async Task WriteLineAsync(string value)
        {
            foreach (var output in _items) await output.WriteLineAsync(value);
        }
    }
}