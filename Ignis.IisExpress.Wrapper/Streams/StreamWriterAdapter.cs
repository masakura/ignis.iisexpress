using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Ignis.IisExpress.Wrapper.Streams
{
    public sealed class StreamWriterAdapter : IOutput
    {
        private readonly StreamWriter _writer;

        public StreamWriterAdapter(Stream stream, Encoding encoding) :
            this(new StreamWriter(stream, encoding, 1024, true))
        {
        }

        public StreamWriterAdapter(StreamWriter writer)
        {
            _writer = writer;
        }

        public async Task WriteLineAsync(string value)
        {
            await _writer.WriteLineAsync(value);
            await _writer.FlushAsync();
        }
    }
}