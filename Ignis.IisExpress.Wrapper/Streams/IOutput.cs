using System.Threading.Tasks;

namespace Ignis.IisExpress.Wrapper.Streams
{
    public interface IOutput
    {
        Task WriteLineAsync(string value);
    }
}