using System.IO;
using System.Threading.Tasks;

namespace Ignis.IisExpress.Wrapper.Streams
{
    /// <summary>
    ///     ストリームを結合します。
    /// </summary>
    internal sealed class StreamPipe
    {
        private readonly StreamReader _input;
        private readonly OutputCollection _outputs;


        public StreamPipe(StreamReader input) : this(input, new OutputCollection())
        {
        }

        private StreamPipe(StreamReader input, OutputCollection outputs)
        {
            _input = input;
            _outputs = outputs;
        }

        public StreamPipe AppendOutput(IOutput output)
        {
            return new StreamPipe(_input, _outputs.Append(output));
        }

        /// <summary>
        ///     結合したストリームの入力から出力への送信を開始します。
        /// </summary>
        public void Run()
        {
#pragma warning disable 4014
            RunAsync();
#pragma warning restore 4014
        }

        public async Task RunAsync()
        {
            await Task.Yield();

            while (!_input.EndOfStream)
            {
                var line = await _input.ReadLineAsync();
                await _outputs.WriteLineAsync(line);
            }
        }
    }
}