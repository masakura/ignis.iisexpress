using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Ignis.IisExpress.Wrapper.Streams
{
    public sealed class ProcessStreamPipe
    {
        private readonly StreamPipe _standardError;
        private readonly StreamPipe _standardOutput;

        public ProcessStreamPipe(Process process) :
            this(CreateDefaultStandardOutput(process), CreateDefaultStandardError(process))
        {
        }

        private ProcessStreamPipe(StreamPipe standardOutput, StreamPipe standardError)
        {
            _standardOutput = standardOutput;
            _standardError = standardError;
        }

        private static StreamPipe CreateDefaultStandardError(Process process)
        {
            return new StreamPipe(process.StandardError)
                .AppendOutput(new StreamWriterAdapter(Console.OpenStandardError(), Console.OutputEncoding));
        }

        private static StreamPipe CreateDefaultStandardOutput(Process process)
        {
            return new StreamPipe(process.StandardOutput)
                .AppendOutput(new StreamWriterAdapter(Console.OpenStandardOutput(), Console.OutputEncoding));
        }

        public Task RunAsync()
        {
            return Task.WhenAll(_standardOutput.RunAsync(), _standardError.RunAsync());
        }

        public ProcessStreamPipe AppendStandardOutputAndError(IOutput output)
        {
            return new ProcessStreamPipe(_standardOutput.AppendOutput(output), _standardError.AppendOutput(output));
        }

        public ProcessStreamPipe AppendStandardOutput(IOutput output)
        {
            return new ProcessStreamPipe(_standardOutput.AppendOutput(output), _standardError);
        }
    }
}