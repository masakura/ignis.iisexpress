using Ignis.IisExpress.Wrapper.Options;

namespace Ignis.IisExpress.Wrapper.Processes
{
    public static class CliOptionsExtensions
    {
        public static ProcessStore ProcessStore(this CliOptions options)
        {
            return new ProcessStore(options.PidFile);
        }
    }
}