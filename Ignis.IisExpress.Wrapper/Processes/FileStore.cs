using System.IO;

namespace Ignis.IisExpress.Wrapper.Processes
{
    internal sealed class FileStore : IStore
    {
        private readonly string _path;

        public FileStore(string path)
        {
            _path = path;
        }

        public void Save(in int processId)
        {
            using var file = File.CreateText(_path);

            file.Write(processId.ToString());
        }

        public int Load()
        {
            var processId = File.ReadAllText(_path);

            return int.Parse(processId);
        }

        public bool Exists()
        {
            return File.Exists(_path);
        }

        public void Delete()
        {
            File.Delete(_path);
        }
    }
}