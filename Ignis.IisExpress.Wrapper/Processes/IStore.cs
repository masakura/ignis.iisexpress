namespace Ignis.IisExpress.Wrapper.Processes
{
    public interface IStore
    {
        void Save(in int processId);
        int Load();
        bool Exists();
        void Delete();
    }
}