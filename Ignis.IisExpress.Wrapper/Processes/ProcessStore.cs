using System.Diagnostics;

namespace Ignis.IisExpress.Wrapper.Processes
{
    public sealed class ProcessStore
    {
        private readonly IStore _store;

        public ProcessStore(string pidFile) : this(new FileStore(pidFile))
        {
        }

        public ProcessStore(IStore store)
        {
            _store = store;
        }

        public void Save(Process process)
        {
            _store.Save(process.Id);
        }

        public RunningProcess Load()
        {
            if (!_store.Exists()) return RunningProcess.NotRunning();

            var processId = _store.Load();
            return new RunningProcess(processId);
        }

        public void Clear()
        {
            if (_store.Exists()) _store.Delete();
        }
    }
}