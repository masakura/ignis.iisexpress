using System.Diagnostics;

namespace Ignis.IisExpress.Wrapper.Processes
{
    public sealed class RunningProcess
    {
        public RunningProcess(int processId)
        {
            Id = processId;
        }

        public int Id { get; }

        public bool IsRunning
        {
            get
            {
                if (Id < 0) return false;

                try
                {
                    var process = Process.GetProcessById(Id);
                    return !process.HasExited;
                }
                catch
                {
                    // 例外外でプロセスが起動中か調べるの面倒...
                    return false;
                }
            }
        }

        public void Kill()
        {
            Process.GetProcessById(Id).Kill();
        }

        public static RunningProcess NotRunning()
        {
            return new RunningProcess(-1);
        }
    }
}