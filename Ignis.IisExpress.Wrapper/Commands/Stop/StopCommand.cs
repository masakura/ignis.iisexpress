using System;
using System.Threading.Tasks;
using Ignis.IisExpress.Wrapper.Options;
using Ignis.IisExpress.Wrapper.Processes;

namespace Ignis.IisExpress.Wrapper.Commands.Stop
{
    internal sealed class StopCommand : ICommand
    {
        private readonly ProcessStore _processStore;

        public StopCommand(CliOptions options)
        {
            _processStore = options.ProcessStore();
        }

        public async Task RunAsync()
        {
            var process = _processStore.Load();

            if (!process.IsRunning)
            {
                await Console.Error.WriteLineAsync("Warning: IIS Express is not running.");
                return;
            }

            process.Kill();
            _processStore.Clear();
        }
    }
}