using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using Ignis.IisExpress.Wrapper.Options;
using Ignis.IisExpress.Wrapper.Processes;
using Ignis.IisExpress.Wrapper.Streams;

namespace Ignis.IisExpress.Wrapper.Commands.Start
{
    internal sealed class StartCommand : ICommand
    {
        private readonly IisExpressOptions _iisExpressOptions;
        private readonly string _logFile;
        private readonly ProcessStore _processStore;

        public StartCommand(IisExpressCliOptions options)
        {
            _processStore = options.CliOptions.ProcessStore();
            _iisExpressOptions = options.IisExpressOptions;
            _logFile = options.CliOptions.LogFile ?? "iisexpress.log";
        }

        public async Task RunAsync()
        {
            VerifyAlreadyRunning();

            var iisExpress = KickIisExpress();

            if (iisExpress == null) return;

            _processStore.Save(iisExpress);

            await using var log = CreateLogWriter();

            try
            {
                var pipe = new ProcessStreamPipe(iisExpress);
                if (log != null) pipe = pipe.AppendStandardOutputAndError(new StreamWriterAdapter(log));

                await pipe.RunAsync();
            }
            finally
            {
                _processStore.Clear();
            }
        }

        private StreamWriter CreateLogWriter()
        {
            var directory = Path.GetDirectoryName(_logFile);
            if (!string.IsNullOrEmpty(directory) &&  !Directory.Exists(directory))
                Directory.CreateDirectory(directory);
            return File.CreateText(_logFile);
        }

        private void VerifyAlreadyRunning()
        {
            if (_processStore.Load().IsRunning)
                throw new InvalidOperationException("IIS Express is already running.");
        }

        private Process KickIisExpress()
        {
            var info = new ProcessStartInfo(@"C:\Program Files (x86)\IIS Express\iisexpress.exe")
            {
                CreateNoWindow = true,
                UseShellExecute = false,
                RedirectStandardOutput = true,
                RedirectStandardError = true
            };
            foreach (var arg in _iisExpressOptions.Arguments) info.ArgumentList.Add(arg);

            return Process.Start(info);
        }
    }
}