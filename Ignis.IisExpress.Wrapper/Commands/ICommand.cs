using System.Threading.Tasks;

namespace Ignis.IisExpress.Wrapper.Commands
{
    internal interface ICommand
    {
        Task RunAsync();
    }
}