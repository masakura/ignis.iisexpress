using System;
using Ignis.IisExpress.Wrapper.Commands.Help;
using Ignis.IisExpress.Wrapper.Commands.Start;
using Ignis.IisExpress.Wrapper.Commands.Stop;
using Ignis.IisExpress.Wrapper.Options;

namespace Ignis.IisExpress.Wrapper.Commands
{
    internal sealed class CommandProvider
    {
        public ICommand Get(IisExpressCliOptions options)
        {
            var commandName = options.CommandName;
            return commandName switch
            {
                "start" => new StartCommand(options),
                "stop" => new StopCommand(options.CliOptions),
                "help" => new HelpCommand(),
                _ => throw new InvalidOperationException($"{commandName} を実行できません。")
            };
        }
    }
}