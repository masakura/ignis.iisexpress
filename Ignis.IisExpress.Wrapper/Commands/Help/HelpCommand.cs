using System;
using System.Threading.Tasks;

namespace Ignis.IisExpress.Wrapper.Commands.Help
{
    internal sealed class HelpCommand : ICommand
    {
        public Task RunAsync()
        {
            Console.WriteLine("iisexpress-cli [CLI options...] [start|stop|help] [IIS Express options...]");
            Console.WriteLine();
            Console.WriteLine(@"iisexpress-cli start /path:C:\path\to\wwwroot");
            Console.WriteLine("\tStart IIS Express.");
            Console.WriteLine("iisexpress-cli stop");
            Console.WriteLine("\tStop IIS Express.");
            Console.WriteLine("iisexpress-cli help");
            Console.WriteLine("\tShow this help.");
            Console.WriteLine();
            Console.WriteLine("CLI Options:");
            Console.WriteLine("\t--pid-file=filepath.pid\tProcess id file.");
            Console.WriteLine("\t--log-file=filepath.log\tiisexpress.exe standard output log file.");

            return Task.CompletedTask;
        }
    }
}