using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;

namespace Ignis.IisExpress.Wrapper.Options
{
    /// <summary>
    ///     コマンドライン引数を、CLI 引数・コマンド・IIS Express 引数に分解します。
    ///     この順序で呼び出してください。
    /// </summary>
    internal sealed class ArgumentsParser
    {
        private ImmutableArray<string> _arguments;

        public ArgumentsParser(IEnumerable<string> arguments)
        {
            _arguments = arguments.ToImmutableArray();
        }

        public ImmutableArray<string> PopCliArguments()
        {
            var cliOptions = _arguments.TakeWhile(item => item.StartsWith("-")).ToImmutableArray();

            _arguments = _arguments.Skip(cliOptions.Length).ToImmutableArray();

            return cliOptions;
        }

        public string PopCommandName()
        {
            if (_arguments.Length <= 0) return null;

            var commandName = _arguments.First();

            _arguments = _arguments.Skip(1).ToImmutableArray();

            return commandName;
        }

        public ImmutableArray<string> PopIisExpressArguments()
        {
            var arguments = _arguments.ToImmutableArray();

            _arguments = Enumerable.Empty<string>().ToImmutableArray();

            return arguments;
        }
    }
}