using System.Collections.Generic;
using System.Collections.Immutable;

namespace Ignis.IisExpress.Wrapper.Options
{
    internal sealed class IisExpressOptions
    {
        public IisExpressOptions(IEnumerable<string> arguments)
        {
            Arguments = arguments.ToImmutableArray();
        }

        public IEnumerable<string> Arguments { get; }
    }
}