using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text.RegularExpressions;

namespace Ignis.IisExpress.Wrapper.Options
{
    public sealed class CliOptions
    {
        private readonly ImmutableDictionary<string, string> _parameters;

        private CliOptions(IDictionary<string, string> parameters)
        {
            _parameters = parameters.ToImmutableDictionary();
        }

        public string PidFile => GetParameter("pid-file");
        public string LogFile => GetParameter("log-file");

        private string GetParameter(string name)
        {
            if (_parameters.TryGetValue(name, out var value)) return value;
            return null;
        }

        private CliOptions Merge(CliOptions options)
        {
            var parameters = new Dictionary<string, string>();

            foreach (var (key, value) in _parameters) parameters[key] = value;
            foreach (var (key, value) in options._parameters) parameters[key] = value;

            return new CliOptions(parameters);
        }

        public static CliOptions Parse(IEnumerable<string> arguments)
        {
            var parameters = arguments
                .Select(argument => Regex.Replace(argument, "^--?", ""))
                .Select(argument => argument.Split("=", 2))
                .ToDictionary(pair => pair[0], pair => pair.ElementAtOrDefault(1));

            return Default().Merge(new CliOptions(parameters));
        }

        private static CliOptions Default()
        {
            return new CliOptions(new Dictionary<string, string>
            {
                {"pid-file", "iisexpress.pid"},
                {"log-file", "iisexpress.log"}
            });
        }
    }
}