using System.Collections.Generic;

namespace Ignis.IisExpress.Wrapper.Options
{
    internal sealed class IisExpressCliOptions
    {
        public IisExpressCliOptions(CliOptions cliOptions, string commandName, IisExpressOptions iisExpressOptions)
        {
            CliOptions = cliOptions;
            CommandName = commandName;
            IisExpressOptions = iisExpressOptions;
        }

        public IisExpressOptions IisExpressOptions { get; }

        public string CommandName { get; }
        public CliOptions CliOptions { get; }

        public static IisExpressCliOptions Parse(IEnumerable<string> arguments)
        {
            var parser = new ArgumentsParser(arguments);
            var cliOptions = CliOptions.Parse(parser.PopCliArguments());
            var commandName = NormalizeCommandName(parser.PopCommandName());
            var iisExpressOptions = new IisExpressOptions(parser.PopIisExpressArguments());

            return new IisExpressCliOptions(cliOptions, commandName, iisExpressOptions);
        }

        private static string NormalizeCommandName(string commandName)
        {
            return commandName ?? "help";
        }
    }
}